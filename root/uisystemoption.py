import dbg
import ui
import snd
import systemSetting
import net
import chat
import app
import localeInfo
import constInfo
import chrmgr
import player
import musicInfo

import uiSelectMusic
import background
import uiPhaseCurtain

MUSIC_FILENAME_MAX_LEN = 25

blockMode = 0

class OptionDialog(ui.ScriptWindow):

	def __init__(self):
		ui.ScriptWindow.__init__(self)
		self.__Initialize()
		self.__Load()

	def __del__(self):
		ui.ScriptWindow.__del__(self)
		print " -------------------------------------- DELETE SYSTEM OPTION DIALOG"

	def __Initialize(self):
		self.tilingMode = 0
		self.titleBar = 0
		self.changeMusicButton = 0
		self.selectMusicFile = 0
		self.ctrlMusicVolume = 0
		self.ctrlSoundVolume = 0
		self.musicListDlg = 0
		self.tilingApplyButton = 0
		self.cameraModeButtonList = []
		self.fogModeButtonList = []
		self.tilingModeButtonList = []
		self.dayModeButtonList = []
		self.snowModeButtonList = []
		self.snowTexturesButtonList = []
		self.curtain = uiPhaseCurtain.PhaseCurtain()
		self.curtain.speed = 0.03
		self.curtain.Hide()
		self.ctrlShadowQuality = 0

	def Destroy(self):
		self.ClearDictionary()

		self.__Initialize()
		print " -------------------------------------- DESTROY SYSTEM OPTION DIALOG"

	def __Load_LoadScript(self, fileName):
		try:
			pyScriptLoader = ui.PythonScriptLoader()
			pyScriptLoader.LoadScriptFile(self, fileName)
		except:
			import exception
			exception.Abort("System.OptionDialog.__Load_LoadScript")

	def __Load_BindObject(self):
		try:
			GetObject = self.GetChild
			self.titleBar = GetObject("titlebar")
			self.selectMusicFile = GetObject("bgm_file")
			self.changeMusicButton = GetObject("bgm_button")
			self.ctrlMusicVolume = GetObject("music_volume_controller")
			self.ctrlSoundVolume = GetObject("sound_volume_controller")
			self.cameraModeButtonList.append(GetObject("camera_short"))
			self.cameraModeButtonList.append(GetObject("camera_long"))
			self.fogModeButtonList.append(GetObject("fog_level0"))
			self.fogModeButtonList.append(GetObject("fog_level1"))
			self.fogModeButtonList.append(GetObject("fog_level2"))
			self.tilingModeButtonList.append(GetObject("tiling_cpu"))
			self.tilingModeButtonList.append(GetObject("tiling_gpu"))
			self.tilingApplyButton=GetObject("tiling_apply")
			self.dayModeButtonList.append(GetObject("night_mode_off"))
			self.dayModeButtonList.append(GetObject("night_mode_on"))
			self.snowModeButtonList.append(GetObject("snow_mode_off"))
			self.snowModeButtonList.append(GetObject("snow_mode_on"))
			self.snowTexturesButtonList.append(GetObject("snow_texture_mode_off"))
			self.snowTexturesButtonList.append(GetObject("snow_texture_mode_on"))
			#self.ctrlShadowQuality = GetObject("shadow_bar")

		except:
			import exception
			exception.Abort("OptionDialog.__Load_BindObject")

	def __Load(self):
		self.__Load_LoadScript("uiscript/systemoptiondialog.py")
		self.__Load_BindObject()

		self.SetCenterPosition()

		self.titleBar.SetCloseEvent(ui.__mem_func__(self.Close))

		self.ctrlMusicVolume.SetSliderPos(float(systemSetting.GetMusicVolume()))
		self.ctrlMusicVolume.SetEvent(ui.__mem_func__(self.OnChangeMusicVolume))

		self.ctrlSoundVolume.SetSliderPos(float(systemSetting.GetSoundVolume()) / 5.0)
		self.ctrlSoundVolume.SetEvent(ui.__mem_func__(self.OnChangeSoundVolume))

		# self.ctrlShadowQuality.SetSliderPos(float(systemSetting.GetShadowLevel()) / 5.0)
		# self.ctrlShadowQuality.SetEvent(ui.__mem_func__(self.OnChangeShadowQuality))

		self.changeMusicButton.SAFE_SetEvent(self.__OnClickChangeMusicButton)

		self.cameraModeButtonList[0].SAFE_SetEvent(self.__OnClickCameraModeShortButton)
		self.cameraModeButtonList[1].SAFE_SetEvent(self.__OnClickCameraModeLongButton)

		self.fogModeButtonList[0].SAFE_SetEvent(self.__OnClickFogModeLevel0Button)
		self.fogModeButtonList[1].SAFE_SetEvent(self.__OnClickFogModeLevel1Button)
		self.fogModeButtonList[2].SAFE_SetEvent(self.__OnClickFogModeLevel2Button)

		self.tilingModeButtonList[0].SAFE_SetEvent(self.__OnClickTilingModeCPUButton)
		self.tilingModeButtonList[1].SAFE_SetEvent(self.__OnClickTilingModeGPUButton)

		self.dayModeButtonList[0].SAFE_SetEvent(self.__OnClickDayModeLightButton)
		self.dayModeButtonList[1].SAFE_SetEvent(self.__OnClickDayModeNightButton)

		self.snowModeButtonList[0].SAFE_SetEvent(self.__OnClickSnowModeOffButton)
		self.snowModeButtonList[1].SAFE_SetEvent(self.__OnClickSnowModeOnButton)

		self.snowTexturesButtonList[0].SAFE_SetEvent(self.__OnClickSnowTexturesOffButton)
		self.snowTexturesButtonList[1].SAFE_SetEvent(self.__OnClickSnowTexturesOnButton)

		self.tilingApplyButton.SAFE_SetEvent(self.__OnClickTilingApplyButton)

		self.__SetCurTilingMode()

		self.__ClickRadioButton(self.fogModeButtonList, constInfo.GET_FOG_LEVEL_INDEX())
		self.__ClickRadioButton(self.cameraModeButtonList, constInfo.GET_CAMERA_MAX_DISTANCE_INDEX())

		self.__ClickRadioButton(self.dayModeButtonList, systemSetting.IsNightMode());
		self.__ClickRadioButton(self.snowModeButtonList, systemSetting.IsSnowOn());
		self.__ClickRadioButton(self.snowTexturesButtonList, systemSetting.IsSnowTextures());

		if musicInfo.fieldMusic==musicInfo.METIN2THEMA:
			self.selectMusicFile.SetText(uiSelectMusic.DEFAULT_THEMA)
		else:
			self.selectMusicFile.SetText(musicInfo.fieldMusic[:MUSIC_FILENAME_MAX_LEN])

	def __OnClickTilingModeCPUButton(self):
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_CPU_TILING_1)
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_CPU_TILING_2)
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_CPU_TILING_3)
		self.__SetTilingMode(0)

	def __OnClickTilingModeGPUButton(self):
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_GPU_TILING_1)
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_GPU_TILING_2)
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_GPU_TILING_3)
		self.__SetTilingMode(1)

	def __OnClickTilingApplyButton(self):
		self.__NotifyChatLine(localeInfo.SYSTEM_OPTION_TILING_EXIT)
		if 0==self.tilingMode:
			background.EnableSoftwareTiling(1)
		else:
			background.EnableSoftwareTiling(0)

		net.ExitGame()

	def __OnClickChangeMusicButton(self):
		if not self.musicListDlg:

			self.musicListDlg=uiSelectMusic.FileListDialog()
			self.musicListDlg.SAFE_SetSelectEvent(self.__OnChangeMusic)

		self.musicListDlg.Open()

	def __ClickRadioButton(self, buttonList, buttonIndex):
		try:
			selButton=buttonList[buttonIndex]
		except IndexError:
			return

		for eachButton in buttonList:
			eachButton.SetUp()

		selButton.Down()

	def __SetTilingMode(self, index):
		self.__ClickRadioButton(self.tilingModeButtonList, index)
		self.tilingMode=index

	def __SetCameraMode(self, index):
		constInfo.SET_CAMERA_MAX_DISTANCE_INDEX(index)
		self.__ClickRadioButton(self.cameraModeButtonList, index)

	def __SetFogLevel(self, index):
		constInfo.SET_FOG_LEVEL_INDEX(index)
		self.__ClickRadioButton(self.fogModeButtonList, index)

	def __OnClickCameraModeShortButton(self):
		self.__SetCameraMode(0)

	def __OnClickCameraModeLongButton(self):
		self.__SetCameraMode(1)

	def __OnClickFogModeLevel0Button(self):
		self.__SetFogLevel(0)

	def __OnClickFogModeLevel1Button(self):
		self.__SetFogLevel(1)

	def __OnClickFogModeLevel2Button(self):
		self.__SetFogLevel(2)

	def __OnChangeMusic(self, fileName):
		self.selectMusicFile.SetText(fileName[:MUSIC_FILENAME_MAX_LEN])

		if musicInfo.fieldMusic != "":
			snd.FadeOutMusic("BGM/"+ musicInfo.fieldMusic)

		if fileName==uiSelectMusic.DEFAULT_THEMA:
			musicInfo.fieldMusic=musicInfo.METIN2THEMA
		else:
			musicInfo.fieldMusic=fileName

		musicInfo.SaveLastPlayFieldMusic()

		if musicInfo.fieldMusic != "":
			snd.FadeInMusic("BGM/" + musicInfo.fieldMusic)

	def OnChangeMusicVolume(self):
		pos = self.ctrlMusicVolume.GetSliderPos()
		snd.SetMusicVolume(pos * net.GetFieldMusicVolume())
		systemSetting.SetMusicVolume(pos)

	def OnChangeSoundVolume(self):
		pos = self.ctrlSoundVolume.GetSliderPos()
		snd.SetSoundVolumef(pos)
		systemSetting.SetSoundVolumef(pos)

	def OnChangeShadowQuality(self):
		pos = self.ctrlShadowQuality.GetSliderPos()
		systemSetting.SetShadowLevel(int(pos / 0.2))

	def OnCloseInputDialog(self):
		self.inputDialog.Close()
		self.inputDialog = None
		return True

	def OnCloseQuestionDialog(self):
		self.questionDialog.Close()
		self.questionDialog = None
		return True

	def OnPressEscapeKey(self):
		self.Close()
		return True

	def Show(self):
		ui.ScriptWindow.Show(self)

	def Close(self):
		self.__SetCurTilingMode()
		self.Hide()

	def __SetCurTilingMode(self):
		if background.IsSoftwareTiling():
			self.__SetTilingMode(0)
		else:
			self.__SetTilingMode(1)

	def __NotifyChatLine(self, text):
		chat.AppendChat(chat.CHAT_TYPE_INFO, text)

	if app.ENABLE_ENVIRONMENT_EFFECT_OPTION:
		def __OnClickDayModeNightButton(self):
			self.curtain.SAFE_FadeOut(self.__DayMode_OnCompleteChangeToDark)
			self.__SetDayMode(1)

		def __OnClickDayModeLightButton(self):
			self.curtain.SAFE_FadeOut(self.__DayMode_OnCompleteChangeToLight)
			self.__SetDayMode(0)

		def __DayMode_OnCompleteChangeToLight(self):
			background.SetEnvironmentData(0)
			self.curtain.FadeIn()

		def __DayMode_OnCompleteChangeToDark(self):
			background.SetEnvironmentData(1)
			self.curtain.FadeIn()

		def __OnClickSnowModeOnButton(self):
			self.__SetSnowMode(1)

		def __OnClickSnowModeOffButton(self):
			self.__SetSnowMode(0)

		def __OnClickSnowTexturesOnButton(self):
			self.__SetSnowTextures(1)

			if background.GetCurrentMapName():
				snow_maps = [
					"metin2_map_a1",
					"metin2_map_b1",
					"metin2_map_c1"
				]

				snow_maps_textures = {
					"metin2_map_a1" : "textureset\snow\snow_metin2_a1.txt",
					"metin2_map_b1" : "textureset\snow\snow_metin2_b1.txt",
					"metin2_map_c1" : "textureset\snow\snow_metin2_c1.txt",
				}

				if str(background.GetCurrentMapName()) in snow_maps:
					background.SetNewTexture(snow_maps_textures[str(background.GetCurrentMapName())])

		def __OnClickSnowTexturesOffButton(self):
			self.__SetSnowTextures(0)

			if background.GetCurrentMapName():
				snow_maps = [
					"metin2_map_a1",
					"metin2_map_b1",
					"metin2_map_c1"
				]

				snow_maps_textures = {
					"metin2_map_a1" : "textureset\metin2_a1.txt",
					"metin2_map_b1" : "textureset\metin2_b1.txt",
					"metin2_map_c1" : "textureset\metin2_c1.txt",
				}

				if str(background.GetCurrentMapName()) in snow_maps:
					background.SetNewTexture(snow_maps_textures[str(background.GetCurrentMapName())])

		def __SetDayMode(self, index):
			self.__ClickRadioButton(self.dayModeButtonList, index)
			systemSetting.SetNightMode(index)

		def __SetSnowMode(self, index):
			self.__ClickRadioButton(self.snowModeButtonList, index)
			background.EnableSnow(index)
			systemSetting.SetSnowMode(index)

		def __SetSnowTextures(self, index):
			self.__ClickRadioButton(self.snowTexturesButtonList, index)
			systemSetting.SetSnowTextures(index)

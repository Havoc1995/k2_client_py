import dbg
import app
import net
import ui
import ime
import snd
import wndMgr
import musicInfo
import serverInfo
import systemSetting
import ServerStateChecker
import localeInfo
import constInfo
import uiCommon
import time
import serverCommandParser
import ime
import uiScriptLocale

#login-interface
import os
import linecache

LOGIN_DELAY_SEC = 0.0
SKIP_LOGIN_PHASE = False
SKIP_LOGIN_PHASE_SUPPORT_CHANNEL = False
FULL_BACK_IMAGE = False

def IsFullBackImage():
	global FULL_BACK_IMAGE
	return FULL_BACK_IMAGE

def IsLoginDelay():
	global LOGIN_DELAY_SEC
	if LOGIN_DELAY_SEC > 0.0:
		return True
	else:
		return False

def GetLoginDelay():
	global LOGIN_DELAY_SEC
	return LOGIN_DELAY_SEC

app.SetGuildMarkPath("test")

class ConnectingDialog(ui.ScriptWindow):
	def __init__(self):
		ui.ScriptWindow.__init__(self)
		self.__LoadDialog()
		self.eventTimeOver = lambda *arg: None
		self.eventExit = lambda *arg: None

	def __del__(self):
		ui.ScriptWindow.__del__(self)

	def __LoadDialog(self):
		try:
			PythonScriptLoader = ui.PythonScriptLoader()
			PythonScriptLoader.LoadScriptFile(self, "UIScript/ConnectingDialog.py")

			self.board = self.GetChild("board")
			self.message = self.GetChild("message")
			self.countdownMessage = self.GetChild("countdown_message")

		except:
			import exception
			exception.Abort("ConnectingDialog.LoadDialog.BindObject")

	def Open(self, waitTime):
		curTime = time.clock()
		self.endTime = curTime + waitTime

		self.Lock()
		self.SetCenterPosition()
		self.SetTop()
		self.Show()

	def Close(self):
		self.Unlock()
		self.Hide()

	def Destroy(self):
		self.Hide()
		self.ClearDictionary()

	def SetText(self, text):
		self.message.SetText(text)

	def SetCountDownMessage(self, waitTime):
		self.countdownMessage.SetText("%.0f%s" % (waitTime, localeInfo.SECOND))

	def SAFE_SetTimeOverEvent(self, event):
		self.eventTimeOver = ui.__mem_func__(event)

	def SAFE_SetExitEvent(self, event):
		self.eventExit = ui.__mem_func__(event)

	def OnUpdate(self):
		lastTime = max(0, self.endTime - time.clock())
		if 0 == lastTime:
			self.Close()
			self.eventTimeOver()
		else:
			self.SetCountDownMessage(self.endTime - time.clock())

	def OnPressExitKey(self):
		#self.eventExit()
		return True

class LoginWindow(ui.ScriptWindow):

	IS_TEST = net.IsTest()

	def __init__(self, stream):
		print "NEW LOGIN WINDOW  ----------------------------------------------------------------------------"
		ui.ScriptWindow.__init__(self)
		net.SetPhaseWindow(net.PHASE_WINDOW_LOGIN, self)
		net.SetAccountConnectorHandler(self)

		self.lastLoginTime = 0
		self.inputDialog = None
		self.connectingDialog = None
		self.stream=stream
		self.isNowCountDown=False
		self.isStartError=False

		self.loadingImage = None

		# @fixme001 BEGIN (timeOutMsg and timeOutOk undefined)
		self.timeOutMsg = False
		self.timeOutOk = False
		# @fixme001 END

	def __del__(self):
		net.ClearPhaseWindow(net.PHASE_WINDOW_LOGIN, self)
		net.SetAccountConnectorHandler(0)
		ui.ScriptWindow.__del__(self)
		print "---------------------------------------------------------------------------- DELETE LOGIN WINDOW"

	def Open(self):
		ServerStateChecker.Create(self)

		print "LOGIN WINDOW OPEN ----------------------------------------------------------------------------"

		self.loginFailureMsgDict={
			#"DEFAULT" : localeInfo.LOGIN_FAILURE_UNKNOWN,

			"ALREADY"	: localeInfo.LOGIN_FAILURE_ALREAY,
			"NOID"		: localeInfo.LOGIN_FAILURE_NOT_EXIST_ID,
			"WRONGPWD"	: localeInfo.LOGIN_FAILURE_WRONG_PASSWORD,
			"FULL"		: localeInfo.LOGIN_FAILURE_TOO_MANY_USER,
			"SHUTDOWN"	: localeInfo.LOGIN_FAILURE_SHUTDOWN,
			"REPAIR"	: localeInfo.LOGIN_FAILURE_REPAIR_ID,
			"BLOCK"		: localeInfo.LOGIN_FAILURE_BLOCK_ID,
			"BESAMEKEY"	: localeInfo.LOGIN_FAILURE_BE_SAME_KEY,
			"NOTAVAIL"	: localeInfo.LOGIN_FAILURE_NOT_AVAIL,
			"NOBILL"	: localeInfo.LOGIN_FAILURE_NOBILL,
			"BLKLOGIN"	: localeInfo.LOGIN_FAILURE_BLOCK_LOGIN,
			"WEBBLK"	: localeInfo.LOGIN_FAILURE_WEB_BLOCK,
			"BADSCLID"	: localeInfo.LOGIN_FAILURE_WRONG_SOCIALID,
			"AGELIMIT"	: localeInfo.LOGIN_FAILURE_SHUTDOWN_TIME,
		}

		self.loginFailureFuncDict = {
			"WRONGPWD"	: self.__DisconnectAndInputPassword,
			"QUIT"		: app.Exit,
		}

		self.SetSize(wndMgr.GetScreenWidth(), wndMgr.GetScreenHeight())
		self.SetWindowName("LoginWindow")

		if not self.__LoadScript(uiScriptLocale.LOCALE_UISCRIPT_PATH + "LoginWindow.py"):
			dbg.TraceError("LoginWindow.Open - __LoadScript Error")
			return

		if musicInfo.loginMusic != "":
			snd.SetMusicVolume(systemSetting.GetMusicVolume())
			snd.FadeInMusic("BGM/"+musicInfo.loginMusic)

		snd.SetSoundVolume(systemSetting.GetSoundVolume())

		# pevent key "[" "]"
		ime.AddExceptKey(91)
		ime.AddExceptKey(93)

		self.Show()

		#login-interface
		self.OnSelectChannel(0)
		self.__LoadSlot()

		app.ShowCursor()

		global SKIP_LOGIN_PHASE
		if SKIP_LOGIN_PHASE:
			if self.isStartError:
				self.PopupNotifyMessage(localeInfo.LOGIN_CONNECT_FAILURE, self.__ExitGame)
				return

	def Close(self):

		if self.connectingDialog:
			self.connectingDialog.Close()
		self.connectingDialog = None

		ServerStateChecker.Initialize(self)

		print "---------------------------------------------------------------------------- CLOSE LOGIN WINDOW "
		#
		# selectMusic이 없으면 BGM이 끊기므로 두개 다 체크한다.
		#
		if musicInfo.loginMusic != "" and musicInfo.selectMusic != "":
			snd.FadeOutMusic("BGM/"+musicInfo.loginMusic)

		## NOTE : idEditLine와 pwdEditLine은 이벤트가 서로 연결 되어있어서
		##        Event를 강제로 초기화 해주어야만 합니다 - [levites]
		self.idEditLine.SetTabEvent(0)
		self.idEditLine.SetReturnEvent(0)
		self.pwdEditLine.SetReturnEvent(0)
		self.pwdEditLine.SetTabEvent(0)

		self.idEditLine = None
		self.pwdEditLine = None
		self.inputDialog = None
		self.connectingDialog = None
		self.loadingImage = None

		self.KillFocus()
		self.Hide()

		self.stream.popupWindow.Close()
		self.loginFailureFuncDict=None

		ime.ClearExceptKey()

		app.HideCursor()

	def __SaveChannelInfo(self):
		try:
			file=old_open("cfg/game/channel.cfg", "w")
			file.write("%d %d %d" % (self.__GetServerID(), self.__GetChannelID(), self.__GetRegionID()))
		except:
			print "LoginWindow.__SaveChannelInfo - SaveError"

	def __LoadChannelInfo(self):
		try:
			file=old_open("cfg/game/channel.cfg")
			lines=file.readlines()

			if len(lines)>0:
				tokens=lines[0].split()

				selServerID=int(tokens[0])
				selChannelID=int(tokens[1])

				if len(tokens) == 3:
					regionID = int(tokens[2])

				return regionID, selServerID, selChannelID

		except:
			print "LoginWindow.__LoadChannelInfo - OpenError"
			return -1, -1, -1

	def __ExitGame(self):
		app.Exit()

	def SetIDEditLineFocus(self):
		if self.idEditLine != None:
			self.idEditLine.SetFocus()

	def SetPasswordEditLineFocus(self):
		if constInfo.ENABLE_CLEAN_DATA_IF_FAIL_LOGIN:
			if self.idEditLine != None: #0000862: [M2EU] 로그인창 팝업 에러: 종료시 먼저 None 설정됨
				self.idEditLine.SetText("")
				self.idEditLine.SetFocus() #0000685: [M2EU] 아이디/비밀번호 유추 가능 버그 수정: 무조건 아이디로 포커스가 가게 만든다

			if self.pwdEditLine != None: #0000862: [M2EU] 로그인창 팝업 에러: 종료시 먼저 None 설정됨
				self.pwdEditLine.SetText("")
		else:
			if self.pwdEditLine != None:
				self.pwdEditLine.SetFocus()

	def OnEndCountDown(self):
		self.isNowCountDown = False
		if localeInfo.IsBRAZIL():
			self.timeOutMsg = True
		else:
			self.timeOutMsg = False
		self.OnConnectFailure()

	def OnConnectFailure(self):

		if self.isNowCountDown:
			return

		snd.PlaySound("sound/ui/loginfail.wav")

		if self.connectingDialog:
			self.connectingDialog.Close()
		self.connectingDialog = None

		if self.timeOutMsg:
			self.PopupNotifyMessage(localeInfo.LOGIN_FAILURE_TIMEOUT, self.SetPasswordEditLineFocus)
		else:
			self.PopupNotifyMessage(localeInfo.LOGIN_CONNECT_FAILURE, self.SetPasswordEditLineFocus)

	def OnHandShake(self):
		if not IsLoginDelay():
			snd.PlaySound("sound/ui/loginok.wav")
			self.PopupDisplayMessage(localeInfo.LOGIN_CONNECT_SUCCESS)

	def OnLoginStart(self):
		if not IsLoginDelay():
			self.PopupDisplayMessage(localeInfo.LOGIN_PROCESSING)

	def OnLoginFailure(self, error):
		if self.connectingDialog:
			self.connectingDialog.Close()
		self.connectingDialog = None

		try:
			loginFailureMsg = self.loginFailureMsgDict[error]
		except KeyError:
			if PASSPOD_MSG_DICT:
				try:
					loginFailureMsg = PASSPOD_MSG_DICT[error]
				except KeyError:
					loginFailureMsg = localeInfo.LOGIN_FAILURE_UNKNOWN + error
			else:
				loginFailureMsg = localeInfo.LOGIN_FAILURE_UNKNOWN  + error


		#0000685: [M2EU] 아이디/비밀번호 유추 가능 버그 수정: 무조건 패스워드로 포커스가 가게 만든다
		loginFailureFunc=self.loginFailureFuncDict.get(error, self.SetPasswordEditLineFocus)

		self.PopupNotifyMessage(loginFailureMsg, loginFailureFunc)

		snd.PlaySound("sound/ui/loginfail.wav")

	def __DisconnectAndInputID(self):
		if self.connectingDialog:
			self.connectingDialog.Close()
		self.connectingDialog = None

		self.SetIDEditLineFocus()
		net.Disconnect()

	def __DisconnectAndInputPassword(self):
		if self.connectingDialog:
			self.connectingDialog.Close()
		self.connectingDialog = None

		self.SetPasswordEditLineFocus()
		net.Disconnect()

	def __LoadScript(self, fileName):
		import dbg
		try:
			pyScrLoader = ui.PythonScriptLoader()
			pyScrLoader.LoadScriptFile(self, fileName)
		except:
			import exception
			exception.Abort("LoginWindow.__LoadScript.LoadObject")
		try:
			GetObject=self.GetChild
			self.loginBoard				= GetObject("LoginBoard")
			self.idEditLine				= GetObject("ID_EditLine")
			self.pwdEditLine			= GetObject("Password_EditLine")
			self.loginButton			= GetObject("LoginButton")
			self.loginExitButton		= GetObject("LoginExitButton")

			#login-interface
			self.resetButton			= GetObject("ResetButton")
			self.registerButton			= GetObject("RegisterButton")
			
			self.channelTab = []
			self.channelTab.append(self.GetChild("Channel1Button"))
			self.channelTab.append(self.GetChild("Channel2Button"))
			self.channelTab.append(self.GetChild("Channel3Button"))
			self.channelTab.append(self.GetChild("Channel4Button"))
			
			self.accountTab = []
			self.accountTab.append(self.GetChild("Account1Login"))
			self.accountTab.append(self.GetChild("Account2Login"))
			self.accountTab.append(self.GetChild("Account3Login"))
			self.accountTab.append(self.GetChild("Account4Login"))
			
			self.accountSaveTab = []
			self.accountSaveTab.append(self.GetChild("Account1Save"))
			self.accountSaveTab.append(self.GetChild("Account2Save"))
			self.accountSaveTab.append(self.GetChild("Account3Save"))
			self.accountSaveTab.append(self.GetChild("Account4Save"))
			
			self.accountControlTab = []
			self.accountControlTab.append(self.GetChild("Account1Control"))
			self.accountControlTab.append(self.GetChild("Account2Control"))
			self.accountControlTab.append(self.GetChild("Account3Control"))
			self.accountControlTab.append(self.GetChild("Account4Control"))

		except:
			import exception
			exception.Abort("LoginWindow.__LoadScript.BindObject")

		# if self.IS_TEST:
			# self.selectConnectButton.Hide()
		# else:
			# self.selectConnectButton.SetEvent(ui.__mem_func__(self.__OnClickSelectConnectButton))

		# self.serverBoard.OnKeyUp = ui.__mem_func__(self.__ServerBoard_OnKeyUp)
		# self.xServerBoard, self.yServerBoard = self.serverBoard.GetLocalPosition()

		# self.serverSelectButton.SetEvent(ui.__mem_func__(self.__OnClickSelectServerButton))
		# self.serverExitButton.SetEvent(ui.__mem_func__(self.__OnClickExitButton))

		self.loginButton.SetEvent(ui.__mem_func__(self.__OnClickLoginButton))
		self.loginExitButton.SetEvent(ui.__mem_func__(self.__OnClickExitButton))

		# self.serverList.SetEvent(ui.__mem_func__(self.__OnSelectServer))

		self.idEditLine.SetReturnEvent(ui.__mem_func__(self.pwdEditLine.SetFocus))
		self.idEditLine.SetTabEvent(ui.__mem_func__(self.pwdEditLine.SetFocus))

		self.pwdEditLine.SetReturnEvent(ui.__mem_func__(self.__OnClickLoginButton))
		self.pwdEditLine.SetTabEvent(ui.__mem_func__(self.idEditLine.SetFocus))

		#login-interface
		self.resetButton.SetEvent(lambda arg="RESET": self.OnWebButtons(arg))
		self.registerButton.SetEvent(lambda arg="REGISTER": self.OnWebButtons(arg))

		self.channelTab[0].SetEvent(lambda arg=0: self.OnSelectChannel(arg))
		self.channelTab[1].SetEvent(lambda arg=1: self.OnSelectChannel(arg))
		self.channelTab[2].SetEvent(lambda arg=2: self.OnSelectChannel(arg))
		self.channelTab[3].SetEvent(lambda arg=3: self.OnSelectChannel(arg))
		self.channelTab[0].Down()
		
		self.accountTab[0].SetEvent(lambda arg=0: self.__LoadSlotInfo(arg))
		self.accountTab[1].SetEvent(lambda arg=1: self.__LoadSlotInfo(arg))
		self.accountTab[2].SetEvent(lambda arg=2: self.__LoadSlotInfo(arg))
		self.accountTab[3].SetEvent(lambda arg=3: self.__LoadSlotInfo(arg))

		self.accountSaveTab[0].SetEvent(lambda arg=0: self.SaveSlot(arg))
		self.accountSaveTab[1].SetEvent(lambda arg=1: self.SaveSlot(arg))
		self.accountSaveTab[2].SetEvent(lambda arg=2: self.SaveSlot(arg))
		self.accountSaveTab[3].SetEvent(lambda arg=3: self.SaveSlot(arg))		
		
		self.accountControlTab[0].SetEvent(lambda arg=0: self.DeleteSlot(arg))
		self.accountControlTab[1].SetEvent(lambda arg=1: self.DeleteSlot(arg))
		self.accountControlTab[2].SetEvent(lambda arg=2: self.DeleteSlot(arg))
		self.accountControlTab[3].SetEvent(lambda arg=3: self.DeleteSlot(arg))

		#login-inferface
		self.SetIDEditLineFocus()
		self.__RequestServerStateList()

		return 1

	def Connect(self, id, pwd):
		if constInfo.SEQUENCE_PACKET_ENABLE:
			net.SetPacketSequenceMode()

		if IsLoginDelay():
			loginDelay = GetLoginDelay()
			self.connectingDialog = ConnectingDialog()
			self.connectingDialog.Open(loginDelay)
			self.connectingDialog.SAFE_SetTimeOverEvent(self.OnEndCountDown)
			self.connectingDialog.SAFE_SetExitEvent(self.OnPressExitKey)
			self.isNowCountDown = True

		else:
			self.stream.popupWindow.Close()
			self.stream.popupWindow.Open(localeInfo.LOGIN_CONNETING, self.SetPasswordEditLineFocus, localeInfo.UI_CANCEL)

		self.stream.SetLoginInfo(id, pwd)
		self.stream.Connect()

	def __OnClickExitButton(self):
		self.stream.SetPhaseWindow(0)

	def __SetServerInfo(self, name):
		net.SetServerInfo(name.strip())

	def PopupDisplayMessage(self, msg):
		self.stream.popupWindow.Close()
		self.stream.popupWindow.Open(msg)

	def PopupNotifyMessage(self, msg, func=0):
		if not func:
			func=self.EmptyFunc

		self.stream.popupWindow.Close()
		self.stream.popupWindow.Open(msg, func, localeInfo.UI_OK)

	def __OnCloseInputDialog(self):
		if self.inputDialog:
			self.inputDialog.Close()
		self.inputDialog = None
		return True

	def OnPressExitKey(self):
		self.stream.popupWindow.Close()
		self.stream.SetPhaseWindow(0)
		return True

	def OnExit(self):
		self.stream.popupWindow.Close()

	def OnUpdate(self):
		ServerStateChecker.Update()

	def EmptyFunc(self):
		pass

	#####################################################################################

	def __GetRegionID(self):
		return 0

	def __GetServerID(self):
		return 1

	def __RefreshServerList(self):
		regionID = self.__GetRegionID()

		if not serverInfo.REGION_DICT.has_key(regionID):
			return

		regionDict = serverInfo.REGION_DICT[regionID]

		# SEVER_LIST_BUG_FIX
		visible_index = 1
		for id, regionDataDict in regionDict.items():
			name = regionDataDict.get("name", "noname")

			try:
				server_id = serverInfo.SERVER_ID_DICT[id]
			except:
				server_id = visible_index

			visible_index += 1

		# END_OF_SEVER_LIST_BUG_FIX

	def __RequestServerStateList(self):
		regionID = self.__GetRegionID()
		serverID = self.__GetServerID()

		try:
			channelDict = serverInfo.REGION_DICT[regionID][serverID]["channel"]
		except:
			print " __RequestServerStateList - serverInfo.REGION_DICT(%d, %d)" % (regionID, serverID)
			return

		ServerStateChecker.Initialize();
		for id, channelDataDict in channelDict.items():
			key=channelDataDict["key"]
			ip=channelDataDict["ip"]
			udp_port=channelDataDict["udp_port"]
			ServerStateChecker.AddChannel(key, ip, udp_port)

		ServerStateChecker.Request()

	def __RefreshServerStateList(self):

		regionID = self.__GetRegionID()
		serverID = self.__GetServerID()

		try:
			channelDict = serverInfo.REGION_DICT[regionID][serverID]["channel"]
		except:
			print " __RequestServerStateList - serverInfo.REGION_DICT(%d, %d)" % (regionID, serverID)
			return

		for channelID, channelDataDict in channelDict.items():
			channelName = channelDataDict["name"]
			channelState = channelDataDict["state"]

			#login-interface
			if channelState != serverInfo.STATE_NONE:
				self.channelTab[channelID-1].SetText("Online")
				self.channelTab[channelID-1].SetColor(51.0, 204.0 , 51.0 , 1.0)
			else:
				self.channelTab[channelID-1].SetText("Offline")
				self.channelTab[channelID-1].SetColor(255.0, 51.0 , 0.0 , 1.0)

	def __GetChannelName(self, regionID, selServerID, selChannelID):
		try:
			return serverInfo.REGION_DICT[regionID][selServerID]["channel"][selChannelID]["name"]
		except KeyError:
			if 9==selChannelID:
				return localeInfo.CHANNEL_PVP
			else:
				return localeInfo.CHANNEL_NORMAL % (selChannelID)

	def NotifyChannelState(self, addrKey, state):
		try:
			stateName=serverInfo.STATE_DICT[state]
		except:
			stateName=serverInfo.STATE_NONE

		regionID=int(addrKey/1000)
		serverID=int(addrKey/10) % 100
		channelID=addrKey%10

		try:
			serverInfo.REGION_DICT[regionID][serverID]["channel"][channelID]["state"] = stateName
			self.__RefreshServerStateList()

		except:
			import exception
			exception.Abort(localeInfo.CHANNEL_NOT_FIND_INFO)

	def __OnClickLoginButton(self):
		id = self.idEditLine.GetText()
		pwd = self.pwdEditLine.GetText()

		if len(id)==0:
			self.PopupNotifyMessage(localeInfo.LOGIN_INPUT_ID, self.SetIDEditLineFocus)
			return

		if len(pwd)==0:
			self.PopupNotifyMessage(localeInfo.LOGIN_INPUT_PASSWORD, self.SetPasswordEditLineFocus)
			return

		self.Connect(id, pwd)

	def OnSelectChannel(self, channel):
		server = 1
		
		addr = serverInfo.REGION_DICT[0][server]['channel'][channel+1]['ip']
		port = serverInfo.REGION_DICT[0][server]['channel'][channel+1]['tcp_port']
		account_addr = serverInfo.REGION_AUTH_SERVER_DICT[0][server]['ip']
		account_port = serverInfo.REGION_AUTH_SERVER_DICT[0][server]['port']
		
		server_name = serverInfo.REGION_DICT[0][server]['name']
		channel_name = serverInfo.REGION_DICT[0][server]['channel'][channel+1]['name']
		
		self.__SetServerInfo("%s - %s" % (server_name, channel_name))
		
		net.SetMarkServer(addr, serverInfo.REGION_DICT[0][server]['channel'][channel+1]['tcp_port'])
		self.stream.SetConnectInfo(addr, port, account_addr, account_port)
		
		for x in range(0, 4):
			if x != channel:
				self.channelTab[x].SetUp()

		self.__SaveChannelInfo()

	def __LoadSlot(self):
		logins = []

		for x in xrange(len(self.accountTab)):
			fileName = "cfg/login/slot_" + str(x+1) + ".cfg"

			if not os.path.exists(os.path.dirname(fileName)):
				os.makedirs(os.path.dirname(fileName))

			if not os.path.exists(fileName):
				open(fileName, 'w').close()

			fd = open(fileName, "r")
			login = fd.readline()
			login.replace("\n", "")
			logins.append(login)
			fd.close()

		for i in xrange(len(self.accountTab)):
			if logins[i] != "":
				self.accountTab[i].SetText(logins[i])
				self.accountTab[i].Enable()
				self.accountControlTab[i].Show()
				self.accountSaveTab[i].Hide()
			else:
				self.accountTab[i].SetText("")
				self.accountTab[i].Disable()
				self.accountControlTab[i].Hide()
				self.accountSaveTab[i].Show()

	def __LoadSlotInfo(self, accid):
		login = linecache.getline("cfg/login/slot_" + str(accid+1) + ".cfg", 1)
		password = linecache.getline("cfg/login/slot_" + str(accid+1) + ".cfg", 2)

		login = login.replace("\n", "")
		password = password.replace("\n", "")

		if login != "" and password != "":
			self.Connect(login, password)
		else:
			self.PopupNotifyMessage("Auf diesem Slot sind keine Daten hinterlegt.")

	def DeleteSlot(self, account):
		f = open("cfg/login/slot_" + str(account+1) + ".cfg", "w")
		f.write("")
		f.close()

		self.__LoadSlot()

		self.PopupNotifyMessage("Slot gel�scht.")

	def SaveSlot(self, slot):
		id = self.idEditLine.GetText()
		pwd = self.pwdEditLine.GetText()

		if id == "":
			self.PopupNotifyMessage("Benutzername oder Passwort unvollst�ndig.")
			return

		if pwd == "":
			self.PopupNotifyMessage("Benutzername oder Passwort unvollst�ndig.")
			return

		f = open("cfg/login/slot_" + str(slot+1) + ".cfg", "w")
		f.write (id +"\n")
		f.write (pwd)
		f.close()

		self.PopupNotifyMessage("Deine Login-Daten wurden gespeichert.")
		self.__LoadSlot()

	def OnWebButtons(self, arg):
		url = ""
		if arg == "RESET":
			url = "http://board.kalisto2.org/"
		elif arg == "REGISTER":
			url = "http://board.kalisto2.org/"

		os.startfile(url)

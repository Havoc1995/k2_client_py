import net, constInfo, ui, background, time, chat
	###################################################################################################################
	# Skype: sacadatt.amazon 
	####### commit test
	###################################################################################################################
class Titan2_Channel_Changer(ui.Window):
	def __init__(self):
		ui.Window.__init__(self)

	def __del__(self):
		ui.Window.__del__(self)

	def Open(self):
		self.vegas_change = ui.BoardWithTitleBar()
		self.vegas_change.AddFlag("movable")
		self.vegas_change.AddFlag("float")
		self.vegas_change.SetTitleName("Channel Switcher") # Title name
		self.vegas_change.SetCloseEvent(self.Close)
		
		x = 0
		
		self.channel_list = []
		
		for ch in xrange(2): # Sets the number of ch's, and we will automatically add buttons.
			channel_button = ui.Button()
			channel_button.SetParent(self.vegas_change) 
			channel_button.SetSize(100, 100)
			channel_button.SetPosition(25, 38 + (40 * x + x))

			
			channel_button.SetUpVisual("d:/ymir work/ui/public/large_button_01.sub")
			channel_button.SetOverVisual("d:/ymir work/ui/public/large_button_02.sub")
			channel_button.SetDownVisual("d:/ymir work/ui/public/large_button_03.sub")
			channel_button.SetEvent(ui.__mem_func__(self.change_channel), ch) # Apply function + ch , function ch1 / ch2 etc.
			
			channel_button.SetText("Channel " + str(ch+1))
			channel_button.Show()
			
			self.channel_list.append(channel_button)
			x = x + 1
		
		self.vegas_change.SetSize(140, 125)
#		self.vegas_change.SetSize(150, 40 + (40 * x + x)) 
		self.vegas_change.SetCenterPosition()
		self.vegas_change.Show()
		
	def protect_maps(self):
		protect_list = [
			"season99/new_map_ox",
			"maps_dungeon/devils_zone",
			"maps_dungeon/dt_zone",
			"maps_vegas/wedding_zone",
			"maps_dungeon/spider_3",  
			"maps_vegas/duel_zone",
		]
		if str(background.GetCurrentMapName()) in protect_list:
			return TRUE
		return FALSE    		
		
	def change_channel(self, ch):
		if self.protect_maps():  #Block change in some maps who are in <protect_list>.
			chat.AppendChat(1, "[Channel Switcher] Du kannst den Channel auf dieser Map nicht wechseln!")
			return	
		elif time.clock() >= constInfo.change_time:			
			self.Close()
			net.SetServerInfo("Kalisto2.org Channel %d" % int(ch+1)) # Set ch name under the minimap and you're on.
#			chat.AppendChat(chat.CHAT_TYPE_INFO, "[Channel Switcher] Du hast den Channel erfolgreich gewechselt!")
			net.SendChatPacket("/ch %d" % int(ch+1)) # Application change control ch + 1 which are added in <xrange(4)>.
			constInfo.change_time = time.clock() + 10 # After pressing the button, the system adds 10 seconds to hold.
		else:
			chat.AppendChat(chat.CHAT_TYPE_INFO, "[Channel Switcher] Du kannst den Channel nur alle 10 Sekunden wechseln!")		
		
	def	Close(self):
		self.vegas_change.Hide()
	
import ui
import player
import chat
import chr

class PetSystemIncubator(ui.ScriptWindow):
	def __init__(self, new_pet):
		ui.ScriptWindow.__init__(self)
		self.__LoadWindow()
		self.LoadPetIncubatorImg(new_pet)

	def __del__(self):
		ui.ScriptWindow.__del__(self)

	def Show(self):
		ui.ScriptWindow.Show(self)

	def Close(self):
		self.Hide()

	def __LoadWindow(self):
		try:
			pyScrLoader = ui.PythonScriptLoader()
			pyScrLoader.LoadScriptFile(self, "uiscript/PetHatchingWindow.py")
		except:
			import exception
			exception.Abort("PetHatchingWindow.LoadWindow.LoadObject")
			
		try:
			self.board = self.GetChild("board")
			self.boardtitle = self.GetChild("PetHatching_TitleBar")
			
			self.petimg = self.GetChild("HatchingItemSlot")
			self.petname = self.GetChild("pet_name")
			self.HatchingButton = self.GetChild("HatchingButton")

			self.boardtitle.SetCloseEvent(ui.__mem_func__(self.Close))
			self.HatchingButton.SetEvent(ui.__mem_func__(self.RequestHatching,))

		except:
			import exception
			exception.Abort("PetHatchingWindow.LoadWindow.BindObject")

	def LoadPetIncubatorImg(self, new_pet):
		petarryname = ["Affenei", "Spinnenei", "Razadorei", "Nemeresei"]
		petarryimg = [55701, 55702, 55703, 55704]

		self.petimg.SetItemSlot(0,petarryimg[int(new_pet)], 0)
		self.petimg.SetAlwaysRenderCoverButton(0, TRUE)

	def RequestHatching(self):
		if self.petname.GetText() == "" or len(self.petname.GetText()) < 4 or len(self.petname.GetText()) > 12:
			chat.AppendChat(chat.CHAT_TYPE_INFO, "[Petsystem] Der Name ihres Pets darf nicht kleiner als 4 und gro�er als 12 Zeichen sein!")
			return

		if player.GetElk() < 100000:
			chat.AppendChat("[Petsystem] Sie haben nicht gen�gend Geld f�r dein Pet.")
			return

		chat.AppendChat(chat.CHAT_TYPE_INFO, "[Petsystem] Der Petname %s wurde ihrem neuem Spr�ssling erfolgreich vergeben!" % (self.petname.GetText()))
		chr.RequestPetName(self.petname.GetText())
		self.Close()

	def OnPressEscapeKey(self):
		self.Close()
		return TRUE

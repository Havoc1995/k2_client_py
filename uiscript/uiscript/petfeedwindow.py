import uiScriptLocale


EXP_Y_ADD_POSITION = 0

window = {
	"name" : "PetFeedWindow",
	"style" : ("movable", "float",),
	
	"x" : SCREEN_WIDTH - 500 + 328 -148,
	"y" : SCREEN_HEIGHT - 595 + 240,	

	"width" : 130,
	"height" : 170,

	"children" :
	(
		{
			"name" : "board",
			"type" : "board",
			"style" : ("attach",),

			"x" : 0,
			"y" : 0,

			"width" : 130,
			"height" : 170,

			"children" :
			(
				{
					"name" : "PetFeed_TitleBar",
					"type" : "titlebar",
					"style" : ("attach",),

					"x" : 0,
					"y" : 0,

					"width" : 128,
					
					"children" :
					(
						{ "name":"TitleName", "type":"text", "x":0, "y":0, "text":"F�ttern", "all_align":"center" },
					),
				},

				{
					"name" : "Pet_Feed_Area",
					"type" : "window",
					"style" : ("attach",),
					
					"x" : 0,
					"y" : 26,
					
					"width" : 128,
					"height" : 144,
					"children" :
					(
						{
							"name" : "FeedItemSlot",
							"type" : "grid_table",

							"x" : 17,
							"y" : 4,

							"start_index" : 0,
							"x_count" : 3,
							"y_count" : 3,
							"x_step" : 32,
							"y_step" : 32,

							"image" : "d:/ymir work/ui/public/Slot_Base.sub"
						},

						{
							"name" : "FeedButton",
							"type" : "button",

							"x" : 44,
							"y" : 110,

							"text" : "Go!",

							"default_image" : "d:/ymir work/ui/public/small_button_01.sub",
							"over_image" : "d:/ymir work/ui/public/small_button_02.sub",
							"down_image" : "d:/ymir work/ui/public/small_button_03.sub",
						},
					),						
					
				},
			),
		},
	),
}

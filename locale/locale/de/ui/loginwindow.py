import uiScriptLocale

LOCALE_PATH = uiScriptLocale.LOGIN_PATH
SERVER_BOARD_HEIGHT = 220 + 180
SERVER_LIST_HEIGHT = 171 + 180

window = {
	"name" : "LoginWindow",
	"sytle" : ("movable",),

	"x" : 0,
	"y" : 0,

	"width" : SCREEN_WIDTH,
	"height" : SCREEN_HEIGHT,

	"children" :
	(
		## Board
		{
			"name" : "bg",
			"type" : "image",
			"x" : SCREEN_WIDTH / 2 - 1920 / 2,
			"y" : SCREEN_HEIGHT / 2 - 1080 / 2,
			"image" : "locale/de/ui/login.jpg",
		},
		
		{
			"name" : "LoginBoard",
			"type" : "expanded_image",
			"x" : SCREEN_WIDTH / 2 - 400, 
			"y" : SCREEN_HEIGHT / 2 - 400,

			"image" : "locale/de/ui/login/interface/login_bg.tga",
			
			"children" :
			(
				{
					"name" : "LoginButton",
					"type" : "button",

					"x" : 361,
					"y" : 466,

					"default_image" : "locale/de/ui/login/interface/buttons/login_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/login_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/login_default.tga",
				},
				{
					"name" : "LoginExitButton",
					"type" : "button",

					"x" : 361,
					"y" : 568,

					"default_image" : "locale/de/ui/login/interface/buttons/exit_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/exit_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/exit_default.tga",
				},
				{
					"name" : "ResetButton",
					"type" : "button",

					"x" : 324,
					"y" : 509,

					"default_image" : "locale/de/ui/login/interface/buttons/reset_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/reset_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/reset_default.tga",
				},
				{
					"name" : "RegisterButton",
					"type" : "button",

					"x" : 319,
					"y" : 533,

					"default_image" : "locale/de/ui/login/interface/buttons/register_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/register_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/register_default.tga",
				},
	
				{
					"name" : "ID_EditLine",
					"type" : "editline",

					"x" : 360,
					"y" : 360,

					"width" : 120,
					"height" : 18,

					"input_limit" : 16,
					"enable_codepage" : 0,

					"a" : 1.0,
				},
				{
					"name" : "Password_EditLine",
					"type" : "editline",

					"x" : 360,
					"y" : 412,

					"width" : 120,
					"height" : 18,
					
					"input_limit" : 16,
					"secret_flag" : 1,
					"enable_codepage" : 0,

					"a" : 1.0,
				},
				
				##ACCOUNTS
				{
					"name" : "Account1Login",
					"type" : "button",

					"x" : 590,
					"y" : 322,

					"default_image" : "locale/de/ui/login/interface/buttons/acc1_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/acc1_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/acc1_default.tga",
					
					"children":
					(
						{
							"name" : "Account1Save",
							"type" : "button",

							"x" : 45,
							"y" : 9,

							"default_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/save_over.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
						},
						{
							"name" : "Account1Control",
							"type" : "button",

							"x" : 135,
							"y" : 13,

							"default_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/del.tga",

							"tooltip_text" : "L�schen",
						},
					),
				},
				{
					"name" : "Account2Login",
					"type" : "button",

					"x" : 590,
					"y" : 370,

					"default_image" : "locale/de/ui/login/interface/buttons/acc2_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/acc2_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/acc2_default.tga",
					
					"children":
					(
						{
							"name" : "Account2Save",
							"type" : "button",

							"x" : 45,
							"y" : 9,

							"default_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/save_over.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
						},
						{
							"name" : "Account2Control",
							"type" : "button",

							"x" : 135,
							"y" : 13,

							"default_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/del.tga",

							"tooltip_text" : "L�schen",
						},
					),
				},
				{
					"name" : "Account3Login",
					"type" : "button",

					"x" : 590,
					"y" : 418,

					"default_image" : "locale/de/ui/login/interface/buttons/acc3_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/acc3_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/acc3_default.tga",
					
					"children":
					(
						{
							"name" : "Account3Save",
							"type" : "button",

							"x" : 45,
							"y" : 9,

							"default_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/save_over.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
						},
						{
							"name" : "Account3Control",
							"type" : "button",

							"x" : 135,
							"y" : 13,

							"default_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/del.tga",

							"tooltip_text" : "L�schen",
						},
					),
				},	
				{
					"name" : "Account4Login",
					"type" : "button",

					"x" : 590,
					"y" : 466,

					"default_image" : "locale/de/ui/login/interface/buttons/acc4_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/acc4_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/acc4_default.tga",
					
					"children":
					(
						{
							"name" : "Account4Save",
							"type" : "button",

							"x" : 45,
							"y" : 9,

							"default_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/save_over.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/save_default.tga",
						},
						{
							"name" : "Account4Control",
							"type" : "button",

							"x" : 135,
							"y" : 13,

							"default_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"over_image" : "locale/de/ui/login/interface/buttons/del.tga",
							"down_image" : "locale/de/ui/login/interface/buttons/del.tga",

							"tooltip_text" : "L�schen",
						},
					),
				},
				{
					"name" : "Channel1Button",
					"type" : "radio_button",

					"x" : 67,
					"y" : 322,

					"default_image" : "locale/de/ui/login/interface/buttons/channel1_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/channel1_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/channel1_down.tga",
					
					"text" : "???",
				},
				{
					"name" : "Channel2Button",
					"type" : "radio_button",

					"x" : 67,
					"y" : 370,
					
					"default_image" : "locale/de/ui/login/interface/buttons/channel2_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/channel2_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/channel2_down.tga",
					
					"text" : "???",
				},
				{
					"name" : "Channel3Button",
					"type" : "radio_button",

					"x" : 67,
					"y" : 418,

					"default_image" : "locale/de/ui/login/interface/buttons/channel3_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/channel3_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/channel3_down.tga",
					
					"text" : "???",
				},
				{
					"name" : "Channel4Button",
					"type" : "radio_button",

					"x" : 67,
					"y" : 466,

					"default_image" : "locale/de/ui/login/interface/buttons/channel4_default.tga",
					"over_image" : "locale/de/ui/login/interface/buttons/channel4_over.tga",
					"down_image" : "locale/de/ui/login/interface/buttons/channel4_down.tga",
					
					"text" : "???",
				},				
			),
		},	
	),
}
